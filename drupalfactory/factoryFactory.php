<?php
/**
 * Description of menuFactory
 *
 */
class factoryFactory {
  static function parseYamlFile($yamlFile) {
    $yaml = file_get_contents($yamlFile);
    return self::parseYaml($yaml);
  }
  static function parseYaml($yaml) {
    $config = yaml_parse($yaml);
    $config = self::fixConstantValues($config);
    return $config;
  }

  /**
   * This function "fixes" constant values in a config string.  E.g.
   * MENU_LOCAL_TASK in a YAML file will be loaded as a string.  This function
   * converts it to it's constant value.
   *
   * @param array $config
   */
  static function fixConstantValues(array &$config) {
    
    foreach ($config as $key=>$value) {
      // recurse if array
      if (is_array($value)) {
        $config[$key] = self::fixConstantValues($value);
        continue;
      }
      // check if string and defined as a constant
      if (is_string($value)) {
        if (defined($value)) {
          $config[$key] = constant($value);
        }
      }
    }
    return $config;
  }
}

?>
