<?php
/**
 * Description of menuFactory
 *
 */
class menuFactory extends factoryFactory {

  static function getMenus($config, $parentPath = '', $defaults = array()) {
    
    $configMenus = safeindex($config, 'menus');
    if (empty($configMenus)) return array();

    // later values overwrite previous ones.
    $defaults = array_merge(self::staticDefaults(), $defaults, safeindex($config, 'defaults'));

    $ret = array();
    $weight=0;

    // loop through each menu item and add it to $ret
    foreach ($configMenus as $configMenu) {
      $menu = array_merge($defaults, $configMenu);

      // create path
      if ($menu['path top'] == 'infer-from-title') {
        $menu['path top'] = self::safeForURL($menu['title']);
      }
      if ($menu['path base'] == 'infer-from-parent') {
        $menu['path base'] = $parentPath;
      }
      if ($menu['path'] == 'infer-from-parent-and-title') {
        $menu['path'] = (empty($menu['path base'])?'':$menu['path base'].'/') . $menu['path top'];
      }
      
      // weight
      if ($menu['weight'] == 'by-index')
        $menu['weight'] = $weight++;

      // add to return values
      $path = $menu['path'];
      unset($menu['defaults'], $menu['menus'], $menu['path top'], $menu['path base'], $menu['path']); // clean up temp varaibles
      $ret[$path] = $menu;

      // process this menu's sub-menus
      $ret = array_merge($ret, self::getMenus($menu, $path, $defaults));
    }
    return $ret;
  }

  static function staticDefaults() {
    return self::parseYaml("
      title: default title
      description: default description
      page callback: drupal_get_form
      #page arguments: infer-from-name
      #file: infer-from-page-arguments
      access callback: true
      type: MENU_NORMAL_ITEM
      weight: by-index
      path top: infer-from-title
      path base: infer-from-parent
      path: infer-from-parent-and-title
      menus: []
      defaults: {}
      "
    );
  }

  static function safeForURL($string) {
    // Thank you Symfony ::slugify
    // replace all non letters or digits by -
    $string = preg_replace('/\W+/', '-', $string);
    // trim and lowercase
    $string = strtolower(trim($string, '-'));
    return $string;
  }

  static function pageEditNodeFromNidVariable($variable) {
    $nid = variable_get($variable);
    $node = node_load($nid);

    require_once('modules/node/node.pages.inc');  // load code for next call
    return drupal_get_form($node->type . '_node_form', $node); 
  }

  static function pageEditUserFromUidVariable($variable, $profile=null) {
    $uid = (int) variable_get($variable);
    $user = user_load($uid);

    require_once('modules/user/user.pages.inc');  // load code for next call
    return drupal_get_form('user_profile_form', $user, $profile);
  }
}

?>
