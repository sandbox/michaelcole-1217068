<?php

/**
 * @file Form functions for pages under construction
 */

/**
 * Returns the form
 * @return form array
 */
function configure_underconstruction_form() {
  $form['under_construction'] = array(
      '#title' => t('Under Contstruction'),
      '#type' => 'fieldset',
  );
  $form['under_construction']['text'] = array(
      '#markup' => 'This page is currently under Construction...',
  );

  return $form;
}

/**
 * Themes the form
 * @param <type> $variables
 * @return <type>
 */
function theme_configure_underconstruction_form($variables) {
  $form = $variables['form'];
  return drupal_render_children($form);
}

/**
 * Validates form values
 */
function configure_underconstruction_form_validate($form, &$form_state) {
}

/**
 * Process a valid form
 */
function configure_underconstruction_form_submit($form, &$form_state) {

}

