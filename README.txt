Hello, this is not plug and play code.  Sorry :-(

This code may be interesting to you for these reasons:

1) Sensible defaults - set at API-level and file-level
2) Readable configs
3) Ability to use PHP constants in non-php files

====== Profile and Bootstrap modules ======

I built an installation profile for a series of sites.  Installation profiles
don't serve my needs of configuration (I was having trouble getting the system
bootstrapped with all the modules I needed during configuration).
So I build a "bootstrap" module with a single URL that runs once after install 
to do the configuration.

I was not 100% happy with this code because it's not very OO or resuable.
It could be improved by using "Factory" or configuration classes.  From this
idea arose "Drupal Factory".  The code has some private data sprinkled all over 
it - I'll clean it up if you are interested.

====== Drupal Factory and Configure Modules ======

This was my second attempt at file-based configuration.

Of note is configure_menu() and menu.yaml

The "Configure" module is supposed to create links to configuration pages for
the site.  These forms/pages would be defined in other modules, and configure
would wire them all together.  

"Configure"s hook_menu uses myMenuFactory which subclasses 
"DrupalFactory"s menuFactory.

 - Every time I see Drupal's associative arrays for menus and forms I want to claw my eyes out.
 
 - Clear defaults
   - menuFactory declares a system default
   - menu.yaml overrides that default with file default
   - each of the menus in menu.yaml declare just what they need to.
   
 - note in factoryFactory->fixConstantValues() replaces MENU_LOCAL_TASK with the
   contstant value.  This is used by factoryFactory->parseYaml() which all it's
   sub-classes inherit.

There's alot I did "wrong" in this code, but I hope you get the idea of where
I was going with it.  I'm interested in helping to flesh this out.

Cheers,

Mike

